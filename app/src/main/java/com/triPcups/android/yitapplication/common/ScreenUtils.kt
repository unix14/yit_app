package com.triPcups.android.yitapplication.common

import android.content.res.Resources
import android.util.DisplayMetrics
import android.view.WindowManager


class ScreenUtils {

    companion object{
        val Int.dp: Int
            get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()

        val Float.dp: Int
            get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()


        fun getScreenWidth(windowManager: WindowManager) : Int {
            val displayMetrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.widthPixels
        }

        fun getScreenHeight(windowManager: WindowManager) : Int {
            val displayMetrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.heightPixels
        }

        fun toDp(int: Int) : Int{
            return int.dp
        }
    }
}