package com.triPcups.android.yitapplication.ui.main.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.triPcups.android.yitapplication.R
import com.triPcups.android.yitapplication.models.Image
import kotlinx.android.synthetic.main.hit_image_item.view.*

class ImagesAdapter(private val listener: ImagesAdapterListener) :
    ListAdapter<Image, RecyclerView.ViewHolder>(ImagesDiffCallback()) {

    private var defaultWidth: Int = -1
    private var defaultHeight: Int = -1

    interface ImagesAdapterListener {
        fun onImageClick(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ImageItemVH(
            LayoutInflater.from(parent.context).inflate(
                R.layout.hit_image_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ImageItemVH
        val imageItem = getItem(position)

        holder.bind(imageItem,defaultWidth, defaultHeight)

        holder.itemView.imageItemVhLayout.setOnClickListener {
            listener.onImageClick(position)
        }
    }

    fun setScreenSize(screenWidth: Int, screenHeight: Int) {
        defaultWidth = screenWidth / 2     // represents the minimal width  of each image in our list
        defaultHeight = screenHeight / 8   // represents the default height of each image in our list
        notifyDataSetChanged()
    }

}