package com.triPcups.android.yitapplication.dependency_injection

import com.triPcups.android.yitapplication.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    //Main Screen
    viewModel { MainViewModel(get(), get()) }

}