package com.triPcups.android.yitapplication.local_db

class Constants {

    companion object{

        //Fragment's Tags
        const val MAIN_FRAGMENT_TAG = "MAIN_FRAGMENT_TAG"

        //Networking
        const val BASE_SERVER_URL: String = "https://pixabay.com/api/"
        const val API_KEY = "17050277-545a19b1dbd0844d1eb9ed6c4"
//        const val API_KEY = "17065122-9bd48d3513abcebf8482b6ea2"      //another one
        const val API_PAGINATION_ITEMS_PER_PAGE = 20

        const val IMAGES_ACTIVITY_OFFSCREEN_PAGE_LIMIT = 5

    }
}