package com.triPcups.android.yitapplication.models

enum class ErrorEvent {
    NO_ERROR,
    UNSUCCESSFUL_RESPONSE,
    NO_RESULTS,
    FAILURE,
}