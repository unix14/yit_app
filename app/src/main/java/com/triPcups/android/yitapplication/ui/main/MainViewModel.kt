package com.triPcups.android.yitapplication.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.triPcups.android.yitapplication.common.ProgressData
import com.triPcups.android.yitapplication.local_db.AppSettings
import com.triPcups.android.yitapplication.local_db.Constants
import com.triPcups.android.yitapplication.models.ErrorEvent
import com.triPcups.android.yitapplication.models.Image
import com.triPcups.android.yitapplication.models.PaginatedImagesResult
import com.triPcups.android.yitapplication.network.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Response

class MainViewModel(private val apiService: ApiService, private val appSettings: AppSettings) :
    ViewModel() {

    val progressData = ProgressData()
    val errorEvent = MutableLiveData<ErrorEvent>()
    val imageListData = MutableLiveData<ArrayList<Image>>()
    val paginationStatus = MutableLiveData<Boolean>()
    val lastSearchQuery = MutableLiveData<String>()

    init {
        loadImages()
    }

    fun loadImages(page: Int = 1, query: String? = null) {
        viewModelScope.launch {
            loadImagesProcess(page, query)
        }
    }

    private suspend fun loadImagesProcess(page: Int = 1, query: String? = null) =
        withContext(Dispatchers.IO) {
            progressData.startProgress()
            //get last query search or use the param passed with this function
            val searchQuery = query ?: appSettings.getLastSearchQuery()
            lastSearchQuery.postValue(searchQuery)

            apiService.searchImages(page, searchQuery)
                .enqueue(object : retrofit2.Callback<PaginatedImagesResult> {
                    override fun onResponse(
                        call: Call<PaginatedImagesResult>,
                        response: Response<PaginatedImagesResult>
                    ) {
                        val paginatedResult = response.body()
                        if (response.isSuccessful && paginatedResult != null) {
                            if (!paginatedResult.hits.isNullOrEmpty()) {
                                errorEvent.postValue(ErrorEvent.NO_ERROR)

                                if (query.isNullOrEmpty()) {                //we firstly enter the app or not searching anything
                                    addResultsToList(paginatedResult.hits)
                                } else {                                    // if we are searching
                                    if (page == 1) {                        // we firstly enter the app or just clicked "GO!" btn
                                        imageListData.postValue(paginatedResult.hits)
                                    } else {                                // we need to add the paginated search results to the list
                                        addResultsToList(paginatedResult.hits)
                                    }
                                }

                                //set pagination status
                                paginationStatus.postValue(
                                    page < paginatedResult.totalHits?.toInt()
                                        ?.div(Constants.API_PAGINATION_ITEMS_PER_PAGE)!!
                                )

                                //save last search query to SharedPrefs
                                query?.let { appSettings.setLastSearchQuery(it) }
                            } else {
                                errorEvent.postValue(ErrorEvent.NO_RESULTS)
                            }
                        } else {
                            errorEvent.postValue(ErrorEvent.UNSUCCESSFUL_RESPONSE)
                        }

                        progressData.endProgress()
                    }

                    override fun onFailure(call: Call<PaginatedImagesResult>, t: Throwable) {
                        errorEvent.postValue(ErrorEvent.FAILURE)
                        progressData.endProgress()
                    }
                })
        }

    private fun addResultsToList(images: java.util.ArrayList<Image>) {
        val currentListData = imageListData.value

        if (!currentListData.isNullOrEmpty()) {
            //send list to fragment
            currentListData.addAll(images)
            imageListData.postValue(currentListData)
        } else {
            imageListData.postValue(images)
        }
    }
}