package com.triPcups.android.yitapplication.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PaginatedImagesResult: Serializable {

    @SerializedName("total")
    var total :  String? = null

    @SerializedName("totalHits")
    var totalHits :  String? = null

    @SerializedName("hits")
    var hits : ArrayList<Image> = arrayListOf()
}

class Image : Serializable{
    @SerializedName("id")
    var id :  String? = null

    @SerializedName("webformatURL")
    var webformatURL :  String? = null

    @SerializedName("webformatWidth")
    var webformatWidth :  String? = null

    @SerializedName("webformatHeight")
    var webformatHeight :  String? = null


    @SerializedName("largeImageURL")
    var largeImageURL :  String? = null

    @SerializedName("imageURL")
    var imageURL :  String? = null

    @SerializedName("imageWidth")
    var imageWidth :  String? = null

    @SerializedName("imageHeight")
    var imageHeight :  String? = null
//
//    @SerializedName("imageSize")
//    var imageSize :  String? = null
//
//    @SerializedName("views")
//    var views :  String? = null
//
//    @SerializedName("downloads")
//    var downloads :  String? = null
//
//    @SerializedName("favorites")
//    var favorites :  String? = null
//
//    @SerializedName("likes")
//    var likes :  String? = null
//
//    @SerializedName("comments")
//    var comments :  String? = null
//
//    @SerializedName("user_id")
//    var user_id :  String? = null
}