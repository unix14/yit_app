package com.triPcups.android.yitapplication

import android.app.Application
import com.triPcups.android.yitapplication.dependency_injection.appModule
import com.triPcups.android.yitapplication.dependency_injection.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class YitApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        setupKoin()
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@YitApplication)
            androidLogger()
            modules(appModule, networkModule)
        }
    }

}