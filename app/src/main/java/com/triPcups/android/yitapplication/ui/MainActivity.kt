package com.triPcups.android.yitapplication.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.triPcups.android.yitapplication.R
import com.triPcups.android.yitapplication.common.ScreenUtils
import com.triPcups.android.yitapplication.local_db.Constants
import com.triPcups.android.yitapplication.models.Image
import com.triPcups.android.yitapplication.ui.images_screen.IMAGE_LIST_PARAM
import com.triPcups.android.yitapplication.ui.images_screen.IMAGE_POSITION_PARAM
import com.triPcups.android.yitapplication.ui.images_screen.ImageActivity
import com.triPcups.android.yitapplication.ui.main.MainFragment
import kotlinx.android.synthetic.main.main_activity.*
import java.util.*

class MainActivity : AppCompatActivity(), MainFragment.MainFragmentListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        showMainFragment()
    }

    private fun showMainFragment() {
        showFragment(MainFragment.newInstance(), Constants.MAIN_FRAGMENT_TAG)
    }

    private fun showFragment(fragment: Fragment, tag: String) {
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(
                R.anim.anim_in, 0, 0,
                R.anim.anim_out
            )
            .replace(R.id.mainActContainer, fragment,tag)
            .commit()
    }

    private fun getFragmentByTag(tag: String): Fragment? {
        return supportFragmentManager.findFragmentByTag(tag)
    }

    override fun showMessage(msg: String) {
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show()
    }

    //should have been named "setProgressBarVisibility" but it's already declared final in AppCompatActivity
    override fun toggleProgressBarVisibility(isShown: Boolean) {
        if(isShown){
            mainActPb.visibility = View.VISIBLE
        } else {
            mainActPb.visibility = View.GONE
        }
    }

    override fun startImagesActivity(imagePosition: Int, list: ArrayList<Image>) {
        val imagesAct = Intent(this, ImageActivity::class.java)
        imagesAct.putExtra(IMAGE_POSITION_PARAM, imagePosition)
        imagesAct.putExtra(IMAGE_LIST_PARAM, list)
        startActivity(imagesAct)
    }

    override fun onResume() {
        super.onResume()

        val mainFrag = getFragmentByTag(Constants.MAIN_FRAGMENT_TAG) as MainFragment?
        mainFrag?.let{
            val screenWidth = ScreenUtils.getScreenWidth(windowManager)
            val screenHeight = ScreenUtils.getScreenHeight(windowManager)
            it.setScreenSize(screenWidth,screenHeight)
        }
    }
}