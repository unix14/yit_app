package com.triPcups.android.yitapplication.ui.images_screen

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.triPcups.android.yitapplication.R
import com.triPcups.android.yitapplication.local_db.Constants
import com.triPcups.android.yitapplication.models.Image
import kotlinx.android.synthetic.main.activity_image.*
import java.util.*

const val IMAGE_POSITION_PARAM = "IMAGE_POSITION_PARAM"
const val IMAGE_LIST_PARAM = "IMAGE_LIST_PARAM"

class ImageActivity : FragmentActivity() {

    private var imgPos: Int = 0
    private var imageList: ArrayList<Image>? = null
    private var imageFileUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        onNewIntent(intent)
        initUi()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val myIntent = getIntent()
        imageList = myIntent.getSerializableExtra(IMAGE_LIST_PARAM) as ArrayList<Image>?
        imgPos = myIntent.getIntExtra(IMAGE_POSITION_PARAM, 0)

        val type = intent!!.type
        val action = intent.action
        if (Intent.ACTION_SEND == action && type != null) {
            if (type.startsWith("image/")) {
                val imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM) as Uri

                imageFileUri = imageUri
            }
        }
    }

    private fun initUi() {
        if (!imageList.isNullOrEmpty()) {
            imageActivityViewPager.adapter = ImageSliderPagerAdapter(this, imageList!!)
        } else if (imageFileUri != null) {
            imageActivityViewPager.adapter = ImageSliderPagerAdapter(this, imageFileUri!!)
        }
        imageActivityViewPager.currentItem = imgPos
        imageActivityViewPager.offscreenPageLimit = Constants.IMAGES_ACTIVITY_OFFSCREEN_PAGE_LIMIT      //load 5 fragments(/images) at every given time
    }
}