package com.triPcups.android.yitapplication.common

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexboxLayoutManager

class SafeFlexboxLayoutManager : FlexboxLayoutManager {
    /*
        At first I've tried working with Staggered and regular Grid layout managers
        and found out a lot of challenge managing dynamically the width of each cell in the adapter

        So, relying on the solution Google gave to this scenario where cell's widths should be calculated dynamically
        and according to this StackOverflow post:
        https://stackoverflow.com/questions/2961777/android-linearlayout-horizontal-with-wrapping-children

        I've used FlexboxLayoutManager instead.
        The wrapper class SafeFlexboxLayoutManager was created because of this crash:
        https://stackoverflow.com/questions/56759960/crash-when-dynamically-change-layoutmanager-to-flexboxlayoutmanager
     */

    constructor(context: Context?) : super(context)

    constructor(context: Context?, flexDirection: Int) : super(context, flexDirection)

    constructor(context: Context, flexDirection: Int, flexWrap: Int) : super(
        context,
        flexDirection,
        flexWrap
    )

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    )

    override fun generateLayoutParams(lp: ViewGroup.LayoutParams): RecyclerView.LayoutParams {
        return FlexboxLayoutManager.LayoutParams(lp)
    }
}