package com.triPcups.android.yitapplication.ui.main.list

import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.triPcups.android.yitapplication.R
import com.triPcups.android.yitapplication.models.Image
import kotlinx.android.synthetic.main.hit_image_item.view.*

class ImageItemVH(v: View) : RecyclerView.ViewHolder(v) {

    private var itemLayout: FrameLayout = v.imageItemVhLayout
    private var hitImage: ImageView = v.imageItemVhImageView

    fun bind(image: Image, defaultWidth: Int, defaultHeight: Int) {
        Glide.with(itemView.context)
            .load(image.webformatURL)
            .placeholder(R.drawable.ic_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .override(
                kotlin.math.min(image.webformatWidth?.toInt()!!, defaultWidth),
                defaultHeight
            )
            .fitCenter()
            .into(hitImage)
    }
}