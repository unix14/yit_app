package com.triPcups.android.yitapplication.local_db

import android.content.SharedPreferences

private const val LAST_SEARCH = "LAST_SEARCH"

class AppSettings(private val sharedPreferences: SharedPreferences) {

    fun setLastSearchQuery(prefKey: String) {
        sharedPreferences.edit().putString(LAST_SEARCH, prefKey).apply()
    }

    fun getLastSearchQuery(): String {
        return sharedPreferences.getString(LAST_SEARCH, "") as String
    }
}
