package com.triPcups.android.yitapplication.dependency_injection

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.triPcups.android.yitapplication.local_db.AppSettings
import com.triPcups.android.yitapplication.local_db.Constants
import com.triPcups.android.yitapplication.network.ApiService
import com.triPcups.android.yitapplication.network.AuthInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {

    //LocalDb
    single { provideSharedPreferences(get()) }
    factory { AppSettings(get()) }

    //networking
    single { provideDefaultOkHttpClient() }
    single { provideRetrofit(get()) }
    single { provideApiService(get()) }

}

fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

fun provideSharedPreferences(context: Context): SharedPreferences {
    return context.getSharedPreferences("User", Context.MODE_PRIVATE)
}

fun provideDefaultOkHttpClient(): OkHttpClient {
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY

    val authInterceptor = AuthInterceptor()

    val httpClient = OkHttpClient.Builder().addInterceptor(authInterceptor).addInterceptor(logging)
    return httpClient.build()
}

fun provideRetrofit(client: OkHttpClient): Retrofit {
    val gson = GsonBuilder()
        .setDateFormat("yyyy-MM-dd HH:mm:ss Z")
        .create()

    return Retrofit.Builder()
        .baseUrl(Constants.BASE_SERVER_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
}