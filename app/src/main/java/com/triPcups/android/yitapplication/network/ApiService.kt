package com.triPcups.android.yitapplication.network

import com.triPcups.android.yitapplication.models.PaginatedImagesResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    //Search pics with a query
    @GET(".")       // We use "." in order to indicate that the endpoint url is the same one as the base url
    fun searchImages(@Query("page") page: Int? = null, @Query("q") query: String? = null): Call<PaginatedImagesResult>
}