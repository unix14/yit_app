package com.triPcups.android.yitapplication.common

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

class KeyboardHelper {

    companion object {
        fun hideKeyboard(context: Context?, v: View) {
            v.clearFocus()
            val imm = context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromWindow(v.windowToken, 0)
        }

        fun showKeyboard(context: Context?, v: View) {
            v.requestFocus()
            val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.showSoftInput(v, InputMethodManager.SHOW_FORCED)
        }
    }
}