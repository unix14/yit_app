package com.triPcups.android.yitapplication.ui.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.JustifyContent
import com.triPcups.android.yitapplication.R
import com.triPcups.android.yitapplication.common.InfiniteRecyclerViewScrollListener
import com.triPcups.android.yitapplication.common.KeyboardHelper
import com.triPcups.android.yitapplication.common.SafeFlexboxLayoutManager
import com.triPcups.android.yitapplication.local_db.Constants
import com.triPcups.android.yitapplication.models.ErrorEvent
import com.triPcups.android.yitapplication.models.Image
import com.triPcups.android.yitapplication.ui.main.list.ImagesAdapter
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class MainFragment : Fragment(), ImagesAdapter.ImagesAdapterListener {

    interface MainFragmentListener {
        fun showMessage(msg: String)
        fun toggleProgressBarVisibility(isShown: Boolean)
        fun startImagesActivity(imagePosition: Int, list: ArrayList<Image>)
    }

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var adapter: ImagesAdapter
    private lateinit var infiniteRecyclerViewScrollListener: InfiniteRecyclerViewScrollListener
    private val viewModel by viewModel<MainViewModel>()
    private var listener: MainFragmentListener? = null
    private lateinit var layoutManager: SafeFlexboxLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainFragmentListener) {
            this.listener = context
        } else {
            throw RuntimeException("$context must implement MainFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()
        initObservers()
        initUi()
    }

    private fun initRecycler() {
        layoutManager = SafeFlexboxLayoutManager(context, FlexDirection.ROW)

        adapter = ImagesAdapter(this)

        infiniteRecyclerViewScrollListener = getInfiniteScrollListener()
        mainFragRecycler.addOnScrollListener(infiniteRecyclerViewScrollListener)

        layoutManager.justifyContent = JustifyContent.FLEX_START
        layoutManager.flexWrap = FlexWrap.WRAP

        mainFragRecycler.adapter = adapter
        mainFragRecycler.layoutManager = layoutManager
    }

    private fun initObservers() {
        viewModel.progressData.observe(viewLifecycleOwner, Observer {
            isLoading -> handleLoading(isLoading) })
        viewModel.errorEvent.observe(viewLifecycleOwner, Observer {
                errorEvent -> handleErrorEvents(errorEvent) })
        viewModel.imageListData.observe(viewLifecycleOwner, Observer {
                imageList -> handleList(imageList) })
        viewModel.paginationStatus.observe(viewLifecycleOwner, Observer {
            infiniteRecyclerViewScrollListener.setHaveMoreData(it) })
        viewModel.lastSearchQuery.observe(viewLifecycleOwner, Observer {
            lastSearchQuery -> mainFragEditText.setText(lastSearchQuery) })
    }

    private fun handleLoading(isLoading: Boolean?) {
        isLoading?.let{
            listener?.toggleProgressBarVisibility(it)
        }
    }

    private fun handleList(imageList: ArrayList<Image>?) {
        if(!imageList.isNullOrEmpty()){
            adapter.submitList(imageList)
            infiniteRecyclerViewScrollListener.notifyDataLoaded()
        }
    }

    private fun handleErrorEvents(errorEvent: ErrorEvent?) {
        errorEvent?.let{
            when(it){
                ErrorEvent.UNSUCCESSFUL_RESPONSE -> {
                    listener?.showMessage("We have technical issues, please contact support")
                }
                ErrorEvent.NO_RESULTS -> {
                    listener?.showMessage("We couldn't find any pictures")
                }
                ErrorEvent.FAILURE -> {
                    listener?.showMessage("We currently have issues with our server, please try later")
                }
                ErrorEvent.NO_ERROR -> {}
            }
        }
    }

    private fun initUi() {
        mainFragSearchBtn.setOnClickListener {
            onSearchClick()
        }

        mainFragEditText.doAfterTextChanged {
            if(it.toString().isNotBlank()){
                mainFragSearchBtn.visibility = View.VISIBLE
            } else {
                mainFragSearchBtn.visibility = View.GONE
            }
        }

        mainFragEditText.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                onSearchClick()
            }
            false
        }
    }

    private fun onSearchClick(){
        val query = mainFragEditText.text.toString().trim()
        viewModel.loadImages(1,query)
        KeyboardHelper.hideKeyboard(context,mainFragEditText)
    }

    private fun getInfiniteScrollListener(): InfiniteRecyclerViewScrollListener {
        return object : InfiniteRecyclerViewScrollListener(this.layoutManager) {
            override fun onDataHunger() {}

            override fun requestData(offset: Int) {
                val page = 1 + offset / Constants.API_PAGINATION_ITEMS_PER_PAGE
                val query = mainFragEditText.text.toString().trim()

                viewModel.loadImages(page,query)
            }
        }
    }

    override fun onImageClick(position: Int) {
        val currentList = viewModel.imageListData.value  //not sure about this

        if(!currentList.isNullOrEmpty()){
            listener?.startImagesActivity(position,currentList)
        }
    }

    fun setScreenSize(screenWidth: Int, screenHeight: Int) {
        adapter.setScreenSize(screenWidth,screenHeight)
    }
}