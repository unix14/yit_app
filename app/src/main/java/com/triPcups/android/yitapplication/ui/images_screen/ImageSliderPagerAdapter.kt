package com.triPcups.android.yitapplication.ui.images_screen

import android.net.Uri
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.triPcups.android.yitapplication.models.Image
import java.util.*

class ImageSliderPagerAdapter : FragmentStateAdapter {

    private var imageList: ArrayList<Image>? = null
    private var imageFile: Uri? = null

    constructor(fa: FragmentActivity, ImageList: ArrayList<Image>) : super(fa) {
        this.imageList = ImageList
    }

    constructor(fa: FragmentActivity, imageFile: Uri) : super(fa) {
        this.imageFile = imageFile
    }

    override fun getItemCount(): Int = if (imageList != null) imageList!!.size else 1

    override fun createFragment(position: Int): Fragment {
       return if (imageList.isNullOrEmpty() && imageFile != null) {
            ImageFragment.newInstance(imageFile!!)
        } else {
            ImageFragment.newInstance(imageList!![position])
        }
    }

}