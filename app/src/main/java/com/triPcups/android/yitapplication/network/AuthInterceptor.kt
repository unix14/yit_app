package com.triPcups.android.yitapplication.network

import android.util.Log
import com.triPcups.android.yitapplication.local_db.Constants
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

private const val TAG = "auth_interceptor"

class AuthInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        request = addApiKeyToRequest(request)

        return chain.proceed(request)
    }

    private fun addApiKeyToRequest(request: Request): Request {
        var req = request
        val apiKey = Constants.API_KEY
        Log.d(TAG, "add api Key to Request: $apiKey")

        val originalHttpUrl = req.url()
        val url = originalHttpUrl.newBuilder().addQueryParameter("key", apiKey).build()

        val requestBuilder = req.newBuilder().url(url)
        req = requestBuilder.build()
        return req
    }
}