package com.triPcups.android.yitapplication.ui.main.list

import androidx.recyclerview.widget.DiffUtil
import com.triPcups.android.yitapplication.models.Image

class ImagesDiffCallback : DiffUtil.ItemCallback<Image>() {

    override fun areItemsTheSame(oldItem: Image, newItem: Image): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Image, newItem: Image): Boolean {
        return oldItem.id != null && newItem.id != null && oldItem.id!!.equals(newItem.id!!)
                && oldItem.webformatURL != null && newItem.webformatURL != null && oldItem.webformatURL!!.equals(newItem.webformatURL!!)
                && oldItem.largeImageURL != null && newItem.largeImageURL != null && oldItem.largeImageURL!!.equals(newItem.largeImageURL!!)
    }
}