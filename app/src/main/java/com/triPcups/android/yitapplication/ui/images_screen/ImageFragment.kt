package com.triPcups.android.yitapplication.ui.images_screen

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.triPcups.android.yitapplication.R
import com.triPcups.android.yitapplication.models.Image
import kotlinx.android.synthetic.main.fragment_image.*

private const val IMAGE_PARAM = "IMAGES_PARAM"
private const val FILE_PARAM = "FILE_PARAM"

class ImageFragment : Fragment() {
    private var image: Image? = null
    private var file: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            image = it.getSerializable(IMAGE_PARAM) as Image?
            file = it.getParcelable(FILE_PARAM) as Uri?
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()
    }

    private fun initUi() {
        context?.let{
            val loadedImage = if(file != null) {
                file
            } else {
                image?.largeImageURL
            }

            Glide.with(it)
                .load(loadedImage)
                .placeholder(R.drawable.ic_placeholder)
                .into(imageFragImage)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(hit: Image) =
            ImageFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(IMAGE_PARAM, hit)
                }
            }

        @JvmStatic
        fun newInstance(file: Uri) =
            ImageFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(FILE_PARAM, file)
                }
            }
    }
}